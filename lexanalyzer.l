%{
	#include <string.h>
	char* output ="<html><head><meta http-equiv='content-type' content='text/html; charset=UTF-8'/><meta name='description' content='PostScript Lexer LP-UNAL'/><meta name='keywords' content='' /><meta name='author' content='SEBASTIÁN ALARCÓN' /><link rel='stylesheet' type='text/css' href='style.css' media='screen' /><title>PostScript Lexer LP-UNAL</title></head><body><div class='main-wrapper'><div class='result default'>";
	int numLineas = 1; 
	char* concat(char *s1, char *s2){
		char *result = malloc(strlen(s1)+strlen(s2)+1);
		strcpy(result, s1);
		strcat(result, s2);
		return result;
	}
%}

DIGITO [0-9]

ID \/?[a-zA-Z][a-zA-Z0-9\-_]*

%%



"%%EOF" {
	output = concat(output,"<span class='eof'>");
	output = concat(output,yytext);
	output = concat(output,"</span>");
	//printf("output:  %s\n", output);
}

"%!"[PS]?[^\n]* {
	output = concat(output,"<span class='id_comment'>");
	output = concat(output,yytext);
	output = concat(output,"</span>");
	//printf("Inicio de archivo: %s\n", yytext);
}

"%"[^\n]* {
	output = concat(output,"<span class='cmm'>");
	output = concat(output,yytext);
	output = concat(output,"</span>");
	//printf("Un comentario: %s\n", yytext);
}


"(".*")" {
	output = concat(output,"<span class='str'>");
	output = concat(output,yytext);
	output = concat(output,"</span>");
	//printf("Un string: %s\n", yytext);	
}


"{" {
	output = concat(output,"<span class='lf_brc'>");
	output = concat(output,yytext);
	output = concat(output,"</span>");
	//printf("Left brace: %s\n", yytext);
}

"}" {
	output = concat(output,"<span class='rg_brc'>");
	output = concat(output,yytext);
	output = concat(output,"</span>");
	//printf("Right brace: %s\n", yytext);
}

"[" {
	output = concat(output,"<span class='lf_brckt'>");
	output = concat(output,yytext);
	output = concat(output,"</span>");
	//printf("Left bracket: %s\n", yytext);
}

"]" {
	output = concat(output,"<span class='rg_brckt'>");
	output = concat(output,yytext);
	output = concat(output,"</span>");
	//printf("Right bracket: %s\n", yytext);
}

-?{DIGITO}*"."{DIGITO}+ {
	output = concat(output,"<span class='num_float'>");
	output = concat(output,yytext);
	output = concat(output,"</span>");
	//printf("Un real: %s\n", yytext);
}

-?{DIGITO}+ {
	output = concat(output,"<span class='num_int'>");
	output = concat(output,yytext);
	output = concat(output,"</span>");
	//printf("Un entero: %s\n", yytext);
}



add|sub|mul|div|idiv|mod|abs|neg|ceiling|floor|round|truncate|sqrt|exp|ln|log|sin|cos|atan|rand|srand|rrand {
	output = concat(output,"<span class='art_ope'>");
	output = concat(output,yytext);
	output = concat(output,"</span>");
	//printf("Una palabra clave: %s\n", yytext); 
}

setlinewidth|makefont|restore|rmoveto|rlineto|arc|arcn|arct|arcto|rcurveto|closepath|fill|newpath|setrgbcolor|curveto|scale|scalefont|setfont|moveto|lineto|stroke|findfont|show|showpage|def|where|ifelse|save|grestore|if|gsave|setcmykcolor|for|repeat|translate|rotate {
	output = concat(output,"<span class='res_word'>");
	output = concat(output,yytext);
	output = concat(output,"</span>");
	//printf("Una palabra clave: %s\n", yytext); 
}

{ID} {
	output = concat(output,"<span class='var'>");
	output = concat(output,yytext);
	output = concat(output,"</span>");
	//printf("Un identificador: %s\n", yytext);
}

[\n] {
	output = concat(output,"<br>");
	++numLineas;
}

[ \t]+ {
	output = concat(output," ");
}

. {
	output = concat(output,"<span class='err'>SE PRESENTÓ UN ERROR LÉXICO EN LA LÍNEA ");
	char strLinea[15];
	sprintf(strLinea, "%d", numLineas);
	output = concat(output, strLinea);
	output = concat(output,". Caracter no reconocido: ");
	output = concat(output,yytext);
	output = concat(output,"</span>");
	//printf("Caracter no reconocido: %s\n", );
}

%%

main(int argc, char** argv){
	char* output_name = "output/output.html";

	if(argv[2]!= NULL){
		output_name = concat("output/",argv[2]);
		output_name = concat(output_name,".html");
	}
	++argv, --argc; // se salta el nombre del programa
	
	FILE *f = fopen(output_name, "w");
	if (f == NULL){
			printf("Error opening file!\n");
			exit(1);
		}
	
	if(argc>0){
		yyin = fopen(argv[0], "r");
	}
	else{
		yyin = stdin;
	}

	yylex();
	output = concat(output,"</div></div></body></html>");
	fprintf(f, "%s", output);	
	fclose(f);

}

int yywrap(){
	return 1;
}