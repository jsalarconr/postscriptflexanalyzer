# LÉEME #

Este es el proyecto 1 de la asignatura Lenguajes de Programación 2015-2. 

El proyecto consiste en un analizador léxico usando la herramienta Flex para el lenguaje PostScript.

## ¿Para qué es este repositorio? ##

* Alojar el código escrito por el autor del mismo
* Alojar los ejemplos con los cuales puede ser usada la herramienta
* Instrucciones de instalación y uso

## ¿Como hago la instalación? ##

### Resumen ###

La aplicación está desarrollada en Flex, por lo que contendrá un archivo generado en C y un archivo para ser ejecutado.

En resumen, para usar la aplicación se debe tener instalado Flex en su ordenador, así como un compilador del lenguaje C.

### Clonar el repositorio ###
Lo primero que debe hacer es clonar el repositorio, para ello introduzca el siguiente texto en una terminal:

```
#!git_commands

git clone https://jsalarconr@bitbucket.org/jsalarconr/postscriptflexanalyzer.git
```

### Dependencias ###
Para ejecutar Flex en una distribución basada en Debian como Ubuntu, antes deberá instalar m4 (general purpose macro processor). Puede seguir este [tutorial](http://geeksww.com/tutorials/libraries/m4/installation/installing_m4_macro_processor_ubuntu_linux.php)

### Instalación de Flex ###

Para instalar Flex puede seguir este [tutorial](http://geeksww.com/tutorials/operating_systems/linux/installation/installing_flex_fast_lexical_analyzer_ubuntu_linux.php)


### Ejecutar el proyecto ###
Una vez instalado todo lo necesario, estando situado en la carpeta clonada del repositorio, escriba en un terminal:

```
#!linux_commands

flex lexanalyzer.l
gcc lex.yy.c
./a.out psfile/***filename*** ***outputfilename***
```

***filename*** es el nombre del archivo en formato .ps que sirve como entrada para el analizador léxico.

***outputfilename*** es el nombre con el que se guardará el archivo de salida. Este campo es opcional, pero recuerde que si ya existe, será sobrescrito.


## Guía de funcionamiento ##

* Bajo la carpeta principal del proyecto se encontrarán los archivos principales para ejecutar el analizador léxico. Un archivo .l de especificación en Flex, un archivo .c compilado que es el compilado y un archivo .out que es el archivo ejecutable.

* Bajo la carpeta *output* se guardan los archivos de salida generados por el analizador léxico y también la hoja de estilos definida por el autor (style.css)

* Bajo la carpeta *psfiles* están los ejemplos en formato .ps que podrán ser usados como entrada del analizador léxico

## ¿Con quién contactar? ##

* Sebastián Alarcón. jsalarconr@unal.edu.co